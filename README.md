SinGE Alarm Clock
=================

The add-on provides alarms for XBMC playing either a file or an URL on repeat.

# Features
  - Scheduling for a single day, weekdays, weekends or everyday.
  - Plays either a file or a custom path may be set which may point to any
      media type supported by XBMC including for example web radio URLs

# Notes
  - If the duration is over, XBMC will stop playing regardless of what is
    being played at the moment. This means it may stop something you were 
    playing intentionally in the meantime.

# Credits
  - Based on the stock Kodi alarm clock by remigius42
    https://github.com/remigius42/script.service.alarmclock

  - Icon https://www.vectorstock.com/royalty-free-vector/glowing-neon-line-alarm-clock-icon-isolated-on-vector-27911517

  - Fanart https://www.itl.cat/wallview/mbbwm_clock-wallpaper-clock-black-and-white-background/
   
